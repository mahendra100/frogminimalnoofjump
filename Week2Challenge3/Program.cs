﻿using System;

namespace Week2Challenge3
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start Position");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("End Position");
            int y = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Distance");
            int z = Convert.ToInt32(Console.ReadLine());
            int result = solution(x, y, z);
            Console.WriteLine("Result :" +result);
            Console.ReadLine();
        }

        public static int solution(int X, int Y, int D)
        {
            int p = 0;
            while (X < Y)
            {
                p++;
                X = X + D;
            }
            return p;
        }
    }
}
